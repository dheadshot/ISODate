ISODate
==========

(Win32, Compile with kernel32)

Outputs the (local) date and time in ISO format.

Usage:
  isodate [/F | /?]
Where:
  /F    Show date in a filename-acceptable format
  /?    Show this help.

Copyright (c) 2021 DHeadshot's Software Creations
MIT Licence

