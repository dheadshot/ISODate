//Based on https://stackoverflow.com/a/53458331/4122994
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Windows.h>
//#include <sysinfoapi.h>
#include <WinBase.h> //Lib=kernel32


char majorver[]="1", minorver[]="01", revisionver[]="0000";

char *monthlut[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec",NULL};
char *daylut[] = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat",NULL};

int streq_(char *a, char *b)
{
  unsigned long la, lb;
  la = strlen(a);
  lb = strlen(b);
  if (la != lb) return 0;
  if (memcmp(a,b,la)==0) return 1; else return 0;
}

void wstrtostr(char *astr, WCHAR *bstr)
{
    int i;
    for (i=0;bstr[i]!=0;i++) astr[i] = ((char) (bstr[i] & 255));
}

int main(int argc, char *argv[])
{
    int fileable = 0;
    int pubable = 0;
    if (argc>1)
    {
        if (streq_(argv[1],"/F") || streq_(argv[1],"/f"))
        {
            fileable = 1;
        }
        else if (streq_(argv[1],"/P") || streq_(argv[1],"/p")) pubable = 1;
        else
        {
            printf("DHSC ISODate version %s.%s.%s\nCopyright (c) 2021 DHeadshot's Software Creations\nMIT Licence\nTwitter @DHeadshot\nMastodon @dheadshot@mastodon.social\n",majorver,minorver,revisionver);
            printf("Usage:\n  %s [/F | /P | /?]\nWhere:\n  /F\tShow date in a filename-acceptable format\n  /P\tShow date as RSS PubDate\n  /?\tShow this help.\n",argv[0]);
            if (streq_(argv[1],"/?"))return 0;
            return 1;
        }
    }
    SYSTEMTIME ltime;
    TIME_ZONE_INFORMATION tzi;
    char tziname[32];
    GetLocalTime(&ltime);
    if (fileable) printf("%04d-%02d-%02d_%02d-%02d-%02d-%03d", ltime.wYear, ltime.wMonth, ltime.wDay, ltime.wHour, ltime.wMinute, ltime.wSecond, ltime.wMilliseconds);
    else if (pubable)
    {
        GetTimeZoneInformation(&tzi);
        wstrtostr(tziname, tzi.StandardName);
        int i;
        for (i=0;tziname[i]!=0;i++) if (tziname[i] == ' ') tziname[i] = 0;
        if (streq_(tziname,""))
        {
            if (tzi.Bias < 0) sprintf(tziname,"UTC+%02d:%02d",((0-tzi.Bias)/60),((0-tzi.Bias)%60));
            else if (tzi.Bias > 0) sprintf(tziname,"UTC-%02d:%02d",((0-tzi.Bias)/60),((0-tzi.Bias)%60));
            else sprintf(tziname,"UTC");
        }
        printf("%s, %02d %s %04d %02d:%02d:%02d %s",daylut[ltime.wDayOfWeek], ltime.wDay, monthlut[ltime.wMonth-1], ltime.wYear, ltime.wHour, ltime.wMinute, ltime.wSecond, tziname);
    }
    else printf("%04d-%02d-%02d %02d:%02d:%02d.%03d", ltime.wYear, ltime.wMonth, ltime.wDay, ltime.wHour, ltime.wMinute, ltime.wSecond, ltime.wMilliseconds);
    return 0;
}
